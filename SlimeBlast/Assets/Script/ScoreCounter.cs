﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{
    public static int scoreCount;
    public Text scoreText;
    // Start is called before the first frame update
    void Start()
    {
        scoreText = GetComponent<Text>();
        scoreCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = scoreCount.ToString();
    }

}
