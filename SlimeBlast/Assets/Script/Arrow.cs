﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public int damage = 10;

    // Start is called before the first frame update

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            Enemy enemy = collision.gameObject.GetComponent<Enemy>();
            enemy.hp -= damage;
            Destroy(this.gameObject);
        }

        if (collision.CompareTag("Enemy2"))
        {
            Enemy2 enemy2 = collision.gameObject.GetComponent<Enemy2>();
            enemy2.hp -= damage;
            Destroy(this.gameObject);
        }

        if (collision.CompareTag("Enemy3"))
        {
            Enemy3 enemy3 = collision.gameObject.GetComponent<Enemy3>();
            enemy3.hp -= damage;
            Destroy(this.gameObject);
        }

        if (collision.CompareTag("Border"))
        {
            Destroy(this.gameObject);
        }


    }
}
