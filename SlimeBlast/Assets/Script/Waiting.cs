﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waiting : MonoBehaviour
{
    public GameObject spawner1,spawner2;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(EnableSpawner1());
        StartCoroutine(EnableSpawner2());
    }

    IEnumerator EnableSpawner1()
    {
        yield return new WaitForSeconds(30);
        spawner1.SetActive(true);
    }

    IEnumerator EnableSpawner2()
    {
        yield return new WaitForSeconds(60);
        spawner2.SetActive(true);
    }

}
