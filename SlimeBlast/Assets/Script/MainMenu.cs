﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public void GameScene()
    {
        Application.LoadLevel(1);
    }

    public void CreditScene()
    {
        Application.LoadLevel(2);
    }

    public void Back()
    {
        Application.LoadLevel(0);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
