﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpPowerup : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, 3f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            UpgradePlayer(collision);
        }
    }

    void UpgradePlayer(Collider2D player)
    {
        PlayerHealth health = player.GetComponent<PlayerHealth>();
        health.maxHp += 20;
        health.currentHp = health.maxHp;
    }

}
