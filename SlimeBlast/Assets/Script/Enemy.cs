﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    
    public Transform player;
    public int hp = 50;
    public int damage = 5;
    public float moveSpeed = 2.5f;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Archer").transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = player.position - transform.position;
        direction.Normalize();
        float movement = moveSpeed * Time.deltaTime;
        transform.position = transform.position + (direction * movement);
        if(hp <= 0)
        {
            ScoreCounter.scoreCount += 1;
            Destroy(this.gameObject);
            
        }
    }

    private void FixedUpdate()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            PlayerHealth player = collision.gameObject.GetComponent<PlayerHealth>();
            player.currentHp -= damage;
        }
    }


}
