﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerHealth : MonoBehaviour
{
    public int maxHp = 100;
    public int currentHp;
    public GameOver gameOver;
    public HealthBar healthBar;
    // Start is called before the first frame update
    void Start()
    {
        currentHp = maxHp;
        healthBar.setMaxHp(maxHp);
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.setHealth(currentHp);

        if(currentHp <= 0)
        {
            Time.timeScale = 0;
            gameOver.gameObject.SetActive(true);
            Destroy(this.gameObject);
        }
    }
}
