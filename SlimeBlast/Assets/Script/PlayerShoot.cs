﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public Transform arrowPoint;
    public GameObject arrowObj;
    public Animator anime;
    public float arrowSpd = 10f;
    public float fireRate = 1f;
    private float nextArrow = 0.0f;
    private bool bow;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") && Time.time > nextArrow)
        {
            nextArrow = Time.time + fireRate;
            Shoot();
            bow = true;
            BowAnim();
        }
    }

    
    void Shoot()
    {
        GameObject arrow = Instantiate(arrowObj, arrowPoint.position, arrowPoint.rotation);
        Rigidbody2D rb = arrow.GetComponent<Rigidbody2D>(); 
        rb.AddForce(arrowPoint.up * arrowSpd, ForceMode2D.Impulse);
    }

    private void BowAnim()
    {
        if (bow)
        {
            anime.SetTrigger("shoot");
        }
    }
}
