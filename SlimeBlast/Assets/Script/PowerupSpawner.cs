﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupSpawner : MonoBehaviour
{
    /*public GameObject powerUp;
    public bool notSpawning = false;
    public float spawnTime;
    public float spawnDelay;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnPowerup", spawnTime, spawnDelay);
    }

    public void SpawnPowerup()
    {
        Instantiate(powerUp, transform.position, transform.rotation);
        if (notSpawning)
        {
            CancelInvoke("SpawnPowerup");
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }*/

    public Transform spawnPos;
    public GameObject powerUp;
    public float spawnRate;

    private float waitTime;

    void Start()
    {
        waitTime = spawnRate;
    }

    // Update is called once per frame
    void Update()
    {
        if (waitTime <= 0)
        {
            Instantiate(powerUp, spawnPos.position, Quaternion.identity);
            waitTime = spawnRate;
        }
        else
        {
            waitTime -= Time.deltaTime;
        }
    }

}
