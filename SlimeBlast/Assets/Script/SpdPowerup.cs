﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpdPowerup : MonoBehaviour
{
    
    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, 3f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            UpgradePlayer(collision);
            //UpgradeEnemy(collision);
        }
    }

    void UpgradePlayer(Collider2D player)
    {
        PlayerMovement speed = player.GetComponent<PlayerMovement>();
        speed.moveSpeed += 0.5f;
    }

    /*void UpgradeEnemy(Collider2D enemy)
    {
        Enemy attack = enemy.GetComponent<Enemy>();
        attack.damage += 2;
    }*/
}
