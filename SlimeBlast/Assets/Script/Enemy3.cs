﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3 : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform player;
    public int hp = 100;
    public int damage = 15;
    public float moveSpeed = 3.5f;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Archer").transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = player.position - transform.position;
        direction.Normalize();
        float movement = moveSpeed * Time.deltaTime;
        transform.position = transform.position + (direction * movement);
        if (hp <= 0)
        {
            ScoreCounter.scoreCount += 3;
            Destroy(this.gameObject);
        }
    }

    private void FixedUpdate()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            PlayerHealth player = collision.gameObject.GetComponent<PlayerHealth>();
            player.currentHp -= damage;
        }
        if (collision.collider.CompareTag("Arrow"))
        {
            hp -= 10;
        }
    }
}
