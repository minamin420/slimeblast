﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    public Transform[] spawnPoint;
    public GameObject[] slime;
    int randomSpawn, randomSlime;
    public static bool isSpawning;
    // Start is called before the first frame update
    void Start()
    {
        isSpawning = true;
            InvokeRepeating("SpawnEnemy", 0f, 5f);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnEnemy()
    {
        if (isSpawning)
        {
            randomSpawn = Random.Range(0, spawnPoint.Length);
            randomSlime = Random.Range(0, slime.Length);
            Instantiate(slime[randomSlime], spawnPoint[randomSpawn].position, Quaternion.identity);
        }
    }


}
