﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtkPowerup : MonoBehaviour
{
    public GameObject arrow;
    void Update()
    {
        Destroy(gameObject, 3f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            UpgradePlayer(collision);
        }
    }

    void UpgradePlayer(Collider2D player)
    {
        PlayerShoot shoot = player.GetComponent<PlayerShoot>();
        shoot.fireRate -= 0.1f;

    }
}
